using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemObjMao : MonoBehaviour
{

    [SerializeField] public Item.TipoItem tipoItem;
    [SerializeField] public Item.NomeFerramentaItemId nomeFerramenta;
    [SerializeField] public Item.NomeRecursoItemId nomeRecurso;
    [SerializeField] public Item.NomeConsumivelItemId nomeConsumivel;
    [SerializeField] public Item.NomeArmaItemId nomeArma;
    [SerializeField] public int damage;

}
